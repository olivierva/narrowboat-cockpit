import Vue from 'vue'
import App from './App.vue'

import VueNativeSock from 'vue-native-websocket'
Vue.use(VueNativeSock, 'ws://localhost:9090/entry', {
    format: 'string',
    reconnection: true, // (Boolean) whether to reconnect automatically (false)
    reconnectionAttempts: 5, // (Number) number of reconnection attempts before giving up (Infinity),
    reconnectionDelay: 3000,
});

Vue.config.productionTip = false;

new Vue({
    render: h => h(App)
}).$mount('#app');